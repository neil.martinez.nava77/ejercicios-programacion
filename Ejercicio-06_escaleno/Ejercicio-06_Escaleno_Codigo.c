/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    float lado1 = 0;
    float lado2 = 0;
    float lado3 = 0;
    float perimetro = 0;
    printf("\n**Calculo del perimetro de un triangulo escaleno**\n\n");
    printf("Ingresa el valor del lado 1 del triángulo escaleno\n");
    scanf("%f",&lado1);
    printf("Ingresa el valor del lado 2 del triángulo escaleno\n");
    scanf("%f",&lado2);
    printf("Ingresa el valor del lado 3 del triángulo escaleno\n");
    scanf("%f",&lado3);
    if (lado1 > 0 && lado2 > 0 && lado3 > 0) {
        perimetro = lado1 + lado2 + lado3;
        printf("El perímetro del triángulo escaleno con longitud de lado1 = %.2f, ",lado1);
        printf("lado2 = %.2f y lado3 = %.2f es = %.2f",lado2,lado3,perimetro);
    } else {
        printf("Datos invalidos");
    }

    return 0;
}
