/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    int i = 0;
    int fin = 0;
    int resultado = 0;
    printf("**Ejercicio 2 (Suma consecutivos)**\n\n");
    printf("Ingresa un numero entre 1 y 50\n");
    scanf("%d",&fin);
    if (fin > 0 && fin < 51){
        for (i = 1; i<=fin; i++) {
            resultado = resultado + i;
        }
        printf("La suma de los números consecutivos del 1 hasta el %d es igual a %d", fin, resultado);
    } else {
        printf("Dato ingresado invalido");
    }
    return 0;
}
