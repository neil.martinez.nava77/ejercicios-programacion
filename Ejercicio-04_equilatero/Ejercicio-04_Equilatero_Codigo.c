/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    float lado = 0;
    float perimetro = 0;
    printf("\nIngresa el valor de un lado del triángulo equilátero\n");
    scanf("%f",&lado);
    if (lado > 0){
        perimetro = 3 * lado;
        printf("El perímetro del triángulo equilátero con longitud de lados %.2f es = %.2f",lado,perimetro);
    } else {
        printf("Dato invalido");
    }

    return 0;
}
