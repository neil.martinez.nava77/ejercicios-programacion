/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    float ladoIgual = 0;
    float ladoDiferente = 0;
    float perimetro = 0;
    printf("\nIngresa el valor de los dos lados iguales del triángulo isósceles\n");
    scanf("%f",&ladoIgual);
    printf("\nIngresa el valor del lado restante del triángulo isósceles\n");
    scanf("%f",&ladoDiferente);
    if (ladoIgual > 0 && ladoDiferente > 0) {
        perimetro = (2 * ladoIgual) + ladoDiferente;
        printf("El perímetro del triángulo isósceles con dos lados de longitud %.2f",ladoIgual);
        printf(" y un lado de %.2f es = %.2f",ladoDiferente,perimetro);
    } else {
        printf("Datos invalidos");
    }

    return 0;
}
