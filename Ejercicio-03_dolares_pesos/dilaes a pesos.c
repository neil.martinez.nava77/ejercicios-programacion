/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    float dolares = 0;
    float pesosMexicanos = 0;
    printf("\n Ingresa la cantidad de dolares a convertir a pesos\n");
    scanf("%f",&dolares);
    if (dolares >= 0){
        pesosMexicanos = dolares * 19.95;
        printf("\nLa cantidad de %.0f dolares equivale a %.2f pesos mexicanos\n", dolares, pesosMexicanos);
    } else {
        printf("\nDato válido ingrese un número positivo");
    } 

    return 0;
}
