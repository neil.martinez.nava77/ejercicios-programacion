/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main() {
    int i = 0;
    int numeroPar = 0; 
    for (i=0;i<=100;i++){
        if (i%2 == 0) {
            numeroPar = i;
            printf("El numero %d es par\n",numeroPar);
        }
    }
    return 0;
}